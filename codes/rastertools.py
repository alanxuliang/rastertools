# -*- coding: utf-8 -*-
"""
Raster tools for remote sensing data analysis

@author: Alan Xu
"""

import subprocess
import shutil
from osgeo import gdal


def raster_clip(maskFile, inFile, outFile, resamplingMethod='near',
                nodataValue='NaN', maxmem='5000'):
    """
    for every input inFile, get the same spatial resolution, projection, and
    extent as the input maskFile.

    output is a new raster file: outFile.
    """

    # path2, ext2 = os.path.splitext(fileMask)
    # shpMask = '{}.shp'.format(path2)
    # subprocess.call(['gdaltindex', shpMask, fileMask], shell=True)

    in0 = gdal.Open(maskFile)
    prj0 = in0.GetProjection()
    extent0, res0 = get_raster_extent(in0)
    extent0 = ' '.join(map(str, extent0))
    res0 = ' '.join(map(str, res0))
    size0 = '{} {}'.format(str(in0.RasterXSize), str(in0.RasterYSize))

    in1 = gdal.Open(inFile)
    prj1 = in1.GetProjection()
    extent1, res1 = get_raster_extent(in1)
    extent1 = ' '.join(map(str, extent1))
    res1 = ' '.join(map(str, res1))

    if (prj0 != prj1) or (extent0 != extent1) or (res0 != res1):
        gdalexpression = (
            'gdalwarp -s_srs {} -t_srs {} -te {} -ts {} '
            '-srcnodata {} -dstnodata {} -wm {} -multi -overwrite '
            '-co COMPRESS=LZW -co PREDICTOR=2 -co TILED=YES -co BIGTIFF=YES '
            '-r {} "{}" "{}"').format(
                prj1, prj0, extent0, size0, nodataValue, nodataValue, maxmem,
                resamplingMethod, inFile, outFile)
        subprocess.check_output(gdalexpression, shell=True)
    else:
        shutil.copy(inFile, outFile)

    in0 = None
    in1 = None

    return


def get_raster_extent(in0):
    """
    for every input in0
    return raster extent, and raster resolution
    """
    GT = in0.GetGeoTransform()
    xS = in0.RasterXSize
    yS = in0.RasterYSize
    x1 = GT[0] + 0*GT[1] + 0*GT[2]
    y1 = GT[3] + 0*GT[4] + 0*GT[5]
    x2 = GT[0] + xS*GT[1] + yS*GT[2]
    y2 = GT[3] + xS*GT[4] + yS*GT[5]
    extent0 = [min(x1, x2), min(y1, y2), max(x1, x2), max(y1, y2)]
    res0 = [max(GT[1], GT[4]), max(GT[2], GT[5])]
    return extent0, res0
