import os
import sys
import pytest
from learn_carbon.glearn import gl_setup
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.gaussian_process import GaussianProcess
from sklearn.linear_model import SGDRegressor
from sklearn import svm
import xgboost as xgb


@pytest.fixture(scope='module')
def datafiles():
    os.chdir(sys.path[0])
    # print(os.getcwd())
    dataPath = os.path.join('..', 'data', 'mouila_test1')
    datafiles = dict(
        dataPath=dataPath,
        outPath=os.path.join(dataPath, 'test_out'),
        fileMask=os.path.join(dataPath, "lc.tif"),
        fileY=os.path.join(dataPath, "geo_chm.tif"),
        fileX=[os.path.join(dataPath, r) for r in
               "mouila_hh_all_reg.tif mouila_hv_all_reg.tif "
               "mouila_srtmV3_reg.tif mouila_tm12_reg.tif".split()]
    )
    return datafiles


@pytest.fixture(scope='module')
def datalearny(datafiles):
    datalearny = gl_setup(datafiles['fileMask'], datafiles['fileY'],
                          nameY='Mean Height (m)', resampleMethod='average')
    return datalearny


@pytest.fixture(scope='module')
def datalearnyx(datalearny, datafiles):
    datalearny.import_xsets(
        datafiles['fileX'], datafiles['fileMask'])
    return datalearny


# def test_setupy(datalearny):
#     print(datalearny.profile)
#     assert datalearny.predMask.shape == (394, 300)
#     assert datalearny.trainMask.shape == (394, 300)
#     assert datalearny.trainY.size == 31605
#     assert datalearny.trainX == []
#     # assert 0
#
#
# def test_setupyx(datalearnyx):
#     print(datalearnyx.profile)
#     assert datalearnyx.predMask.shape == (394, 300)
#     assert datalearnyx.trainMask.shape == (394, 300)
#     assert datalearnyx.trainY.size == 18
#     assert len(datalearnyx.trainX) == 7
#     assert len(datalearnyx.trainX[0]) == 18
#     assert len(datalearnyx.predX) == 7
#     assert len(datalearnyx.predX[0]) == 31605
#     # assert 0

# def test_cvRF(datalearnyx):
#     # cv = cross_validation.ShuffleSplit(
#     #     n_samples, n_iter=3, test_size=0.3, random_state=0)
#     nFeature = len(datalearnyx.trainX)
#     model = RandomForestRegressor(n_jobs=-1)
#     params = {
#         'suplearn__n_estimators': [10, 100, 500, 1000],
#         'suplearn__max_features': [
#             nFeature, int(nFeature*2/3), int(nFeature*1/2)],
#         'suplearn__min_samples_leaf': [1, 5, 15],
#     }
#     assert datalearnyx.trainY.shape == (31605,)
#     assert nFeature == 7
#     datalearnyx.sklearn_gridsearch(model, params)
#     assert 0


# def test_predictRF(datalearnyx, datafiles):
#     nFeature = len(datalearnyx.trainX)
#     model = RandomForestRegressor(n_jobs=-1)
#     params = {
#         'suplearn__n_estimators': [10, 100, 500, 1000],
#         'suplearn__max_features': [
#             nFeature, int(nFeature*2/3), int(nFeature*1/2)],
#         'suplearn__min_samples_leaf': [1, 5, 15],
#     }
#     print(type(datalearnyx.trainX))
#     datalearnyx.sklearn_gridsearch(model, params)
#     datalearnyx.sklearn_train()
#     outY = os.path.join(datafiles['outPath'], "test_map_RF.tif"),
#     datalearnyx.sklearn_predict(outY)
#     assert 0


# def test_cvRF_plot(datalearnyx, datafiles):
#     nFeature = len(datalearnyx.trainX)
#     model = RandomForestRegressor(n_jobs=-1)
#     params = {
#         'suplearn__n_estimators': [10, 100, 500, 1000],
#         'suplearn__max_features': [
#             nFeature, int(nFeature*2/3), int(nFeature*1/2)],
#         'suplearn__min_samples_leaf': [1, 5, 15],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_RF.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0


# def test_cvGradientBoost_plot(datalearnyx, datafiles):
#     nFeature = len(datalearnyx.trainX)
#     model = GradientBoostingRegressor()
#     params = {
#         'suplearn__loss': ['lad', 'huber'],
#         'suplearn__n_estimators': [10, 100, 500, 1000],
#         'suplearn__max_features': [
#             nFeature, int(nFeature*2/3), int(nFeature*1/2)],
#         'suplearn__max_depth': [1, 5, 15],
#         'suplearn__subsample': [1, 0.75, 0.5],
#         'suplearn__min_samples_leaf': [1, 5],
#         'suplearn__learning_rate': [0.5, 0.1, 0.01],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_GB.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0


# def test_cvExtraTree_plot(datalearnyx, datafiles):
#     nFeature = len(datalearnyx.trainX)
#     model = ExtraTreesRegressor(n_jobs=-1)
#     params = {
#         'suplearn__n_estimators': [10, 100, 500, 1000],
#         'suplearn__max_features': [
#             nFeature, int(nFeature*2/3), int(nFeature*1/2)],
#         'suplearn__min_samples_leaf': [1, 5, 15],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_ET.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0
#
#
# def test_cvAdaBoost_plot(datalearnyx, datafiles):
#     model = AdaBoostRegressor()
#     params = {
#         'suplearn__n_estimators': [10, 100, 500, 1000],
#         'suplearn__learning_rate': [0.5, 0.1, 0.01],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_AB.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0

# def test_cvGP_plot(datalearnyx, datafiles):
#     model = GaussianProcess()
#     params = {
#         'suplearn__thetaL': [1e-5, 1e-3],
#         'suplearn__thetaU': [1, 0.1],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_GP.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0


# def test_cvSGD_plot(datalearnyx, datafiles):
#     model = SGDRegressor()
#     params = {
#         'suplearn__alpha': [1e-5, 1e-2],
#         'suplearn__l1_ratio': [0.1, 0.3],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_SGD.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0


# def test_cvSVRLinear_plot(datalearnyx, datafiles):
#     model = svm.SVR()
#     params = {
#         'suplearn__C': [1, 10, 100, 1000], 'suplearn__kernel': ['linear']}
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_SVRLinear.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0


# def test_cvSVRRBF_plot(datalearnyx, datafiles):
#     model = svm.SVR()
#     params = {
#         'suplearn__C': [10, 100, 1000, 5000, 10000],
#         'suplearn__epsilon': [2, 1.5, 1, 0.5],
#         'suplearn__gamma': [0.01, 0.001, 0.0001],
#         'suplearn__kernel': ['rbf']}
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_SVRRBF.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0


# def test_cvXGB_plot(datalearnyx, datafiles):
#     model = xgb.XGBRegressor()
#     params = {
#         'suplearn__max_depth': [5, 10, 15],
#         'suplearn__learning_rate': [1, 0.7, 0.5, 0.3],
#         'suplearn__n_estimators': [100, 500, 1000],
#     }
#     datalearnyx.sklearn_gridsearch(model, params)
#
#     outY = os.path.join(datafiles['outPath'], "test_plot_XGB.png")
#     # outY = "test_plot_RF.png"
#     datalearnyx.sklearn_train(outY)
#     assert 0
