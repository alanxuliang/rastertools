from setuptools import setup, find_packages
from codecs import open
from os import path


here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='rastertools',

    version='0.1.0',

    description='Estimate terrestrial carbon stock from remote sensing data.',
    long_description=long_description,

    url='https://gitlab.com/alanxuliang/rastertools',

    author='Alan Xu',
    author_email='bireme@gmail.com',

    license='MIT',

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
    ],

    keywords='Remote Sensing GIS Machine Learning',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    install_requires=[
        'scikit-learn',
        'scipy',
        'numpy',
        'rasterstats',
        'gdal'
    ],
)
